# Springular1st

## Getting started

* Build Springular1st with `mvn clean install spring-boot:run`
* Call Angular with `http://localhost:8080` in the Browser. You should see "Hallo Springular1st".

## Docker
If Docker is running you can use `mvn spring-boot:build-image` to build a docker image. The images name is `backend:1.0-SNAPSHOT`.

You can start the application with
* ```docker run --name springular1st -d -p 8080:8080 backend:1.0-SNAPSHOT```

If Docker is not running you can use `mvn clean install jib:build` to build a docker image.
In this case you need a registry where jib can push the image. So you have to replace `docker.io/javacook` in `backend/pom.xml` with your own registry. After that you can start the app with
* `docker run --name springular1st -d -p 8080:8080 javacook/springular1st`

Note that you can omit `docker.io` in the docker run command since it is default. 